import com.codeborne.selenide.SelenideElement;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.*;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.Assert.assertTrue;

public class TestCase {
    @Before
    public void setProperties() {
        System.setProperty("webdriver.chrome.driver", "E://Home_work2//chrome_driver//chromedriver.exe");
        System.setProperty("selenide.browser", "Chrome");
        open("http://todolistme.net/");
    }


    @Test
    public void testCase1() {
        String todo1 = "todo1";
        String todo2 = "todo2";
        List<SelenideElement> todoElements = $$(By.xpath("//div[@id=\"todolistpanel\"]//span"));
        $(By.xpath("//input[@id=\"newtodo\"]")).setValue(todo1).pressEnter();
        $(By.xpath("//input[@id=\"newtodo\"]")).setValue(todo2).pressEnter();
        $(By.xpath("//span[text()=\""+todo1+"\"]/preceding-sibling::input")).click();
        $(By.xpath("//span[text()=\""+todo2+"\"]/preceding-sibling::input")).click();
        $(By.xpath("//div[@id=\"doneitemspanel\"]//span[text()=\""+todo1+"\"]")).shouldBe(enabled);
        $(By.xpath("//div[@id=\"doneitemspanel\"]//span[text()=\""+todo2+"\"]")).shouldBe(enabled);

        $(By.xpath("//a[@class=\"purge\"]")).click();// Удаляем все элементы Done

        // Проверяем или Done чист
        List<SelenideElement> doneElements =
                $$(By.xpath("//div[@id=\"doneitemspanel\"]//span")).shouldBe(empty);

        // Проверям что все элементы To do что были в начале присутствуют и после тестов
        todoElements.stream().forEach(elem -> elem.shouldBe(enabled));

        sleep(5000);
    }

    @Test
    public void testCase2() {
        String todo1 = "todo1";
        // Получаем все элементы To do перед началом манипуляций со страницой
        List<SelenideElement> todoElements = $$(By.xpath("//div[@id=\"todolistpanel\"]//span"));
        // Дбавляем запись в To do
        $(By.xpath("//input[@id=\"newtodo\"]")).setValue(todo1).pressEnter();
        // Удаляем запись из To do
        $(By.xpath("//span[text()=\""+todo1+"\"]/following-sibling::img")).hover().click();
        // Проверям что все элементы To do что были в начале присутствуют и после тестов
        todoElements.stream().forEach(elem -> elem.shouldBe(enabled));
        // Проверяем что нашей записи нет в Done
        $(By.xpath("//div[@id=\"doneitemspanel\"]//span[text()=\""+todo1+"\"]")).shouldBe(disappear);
        $(By.xpath("//div[@id=\"todolistpanel\"]//span[text()=\""+todo1+"\"]")).shouldBe(disappear);//Проверяем что нашей записи нет в To do

        sleep(3000);
    }

    @Test
    public void testCase3() {
        // Получаем все элементы ло сортировки
        List<SelenideElement> todoElementsBefore = $$(By.xpath("//div[@id=\"todolistpanel\"]//span"));
        // Получаем все строки элементов и сортируем с помощью java
        List<String> todoStringsBefore  = new ArrayList<>();
        todoElementsBefore.stream().forEach(elem -> todoStringsBefore.add(elem.getText()));
        Collections.sort(todoStringsBefore, (String s1, String s2) -> s1.compareTo(s2));

        sleep(3000);
    }


}
